package com.example.thumbfight


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.example.thumbfight.databinding.ActivityMainBinding
import com.example.thumbfight.game.GameManager
import com.example.thumbfight.game.GameManagerCallback
import com.example.thumbfight.game.Player

private const val TAG = "MainActivity"
class MainActivity : AppCompatActivity(), GameManagerCallback {

    private lateinit var binding : ActivityMainBinding
    lateinit var gameManager : GameManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        gameManager = GameManager()
        gameManager.callback = this
        Log.d(TAG,"onCreate()")
    }

    fun onStartGameButtonClicked(view : View){
        gameManager.startGame()
    }
    fun onLeftButtonClicked(view : View){
        gameManager.clickReceived(Player.LEFT)
    }
    fun onRightButtonClicked(view : View){
        gameManager.clickReceived(Player.RIGHT)
    }

    //Start of callbacks
    override fun timeChanged(secsLeft: Int) {
        binding.timeLeftTexiew.text = getString(R.string.time_left_text, secsLeft)
    }

    override fun setplayer(player: Player) {
        binding.PlayerTextView.text = player.name
    }

    override fun scoreChanged(player: Player, score: Int) {
        if(player == Player.LEFT){
            binding.leftScoretextView.text = getString(R.string.score_text,score)
        }else{
            binding.rightScoreTextView.text = getString(R.string.score_text,score)
        }
    }

    override fun showWinner(winners: Array<Player>) {
        var message = getString(R.string.tie_message)
        if(winners.size == 1){
                if(winners.contains(Player.LEFT)){
                    message = getString(R.string.winner_message, Player.LEFT.name)
                }else{
                    message = getString(R.string.winner_message, Player.RIGHT.name)
                }
        }
        AlertDialog.Builder(this)
            .setTitle(R.string.game_over)
            .setMessage(message)
            .setPositiveButton(R.string.ok_Button,null)
            .show()
    }

}