package com.example.thumbfight.game

import android.os.CountDownTimer
import android.util.Log

class GameManager {

    var currentPlayer = Player.LEFT
    var timeRemaining = 0
    var timer : CountDownTimer? = null
    var scoreMap : HashMap<Player, Int> = HashMap()
    var callback :GameManagerCallback? = null

    fun startGame(){
        if(timeRemaining == 0){
        //Reset the time and current player
        timeRemaining = 10
        currentPlayer = Player.LEFT

        //Reset the timer
        timer = object: CountDownTimer(timeRemaining*1000L,1000 ){
            override fun onTick(milisecUntilFinished: Long) {
                Log.d("MANU KM","-------------${milisecUntilFinished}-----------------------------")
                var timeLeft = (milisecUntilFinished/1000).toInt()
                callback?.timeChanged(timeLeft)
                proceessEvent(timeLeft)
            }

            override fun onFinish() {

            }
        }
        //Reset the score
        scoreMap[Player.LEFT] = 0
        scoreMap[Player.RIGHT] = 0

        //Inform the UI
        callback?.setplayer(Player.LEFT)
        callback?.timeChanged(timeRemaining)
        callback?.scoreChanged(Player.LEFT,0)
        callback?.scoreChanged(Player.RIGHT,0)

        //Start the timer

            timer?.start()
        }

    }
    fun clickReceived(player : Player){
        if(currentPlayer == player){
            if(scoreMap[player]!=null){
                scoreMap[player] = scoreMap[player]?.plus(1)?:1
            }
            callback?.scoreChanged(Player.LEFT,scoreMap[Player.LEFT]?:0)
            callback?.scoreChanged(Player.RIGHT,scoreMap[Player.RIGHT]?:0)
        }
    }
    fun proceessEvent(timeLeft : Int){
        //
        if(timeLeft > 0 && timeLeft <=5){
            currentPlayer = Player.RIGHT
            callback?.setplayer(Player.RIGHT)
        }
        //Game Over
        if(timeLeft < 1){
            val leftPlayerScore = scoreMap[Player.LEFT]?:0
            val rightPlayerScore = scoreMap[Player.RIGHT]?:0
            timeRemaining = 0


            if(leftPlayerScore > rightPlayerScore){
                callback?.showWinner(arrayOf(Player.LEFT))
            }else if(rightPlayerScore > leftPlayerScore){
                callback?.showWinner(arrayOf(Player.RIGHT))
            }else{
                callback?.showWinner(arrayOf(Player.RIGHT,Player.RIGHT))
            }
        }
    }

}

interface GameManagerCallback{
    fun timeChanged(secsLeft : Int)
    fun setplayer(player : Player)
    fun scoreChanged(player: Player , score :Int)
    fun showWinner(winners : Array<Player>)
}
enum class Player{
    LEFT,RIGHT
}